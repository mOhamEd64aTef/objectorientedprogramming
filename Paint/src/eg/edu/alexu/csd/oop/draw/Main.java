package eg.edu.alexu.csd.oop.draw;

import eg.edu.alexu.csd.oop.draw.controller.Controller;

import java.awt.EventQueue;

public class Main {

  /**
   * Launch the application.
   *
   * @param args
   *          the arguments
   */

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          Controller controller = new Controller();
          controller.startApplication();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

}
