package eg.edu.alexu.csd.oop.draw.controller;

import eg.edu.alexu.csd.oop.draw.ClassFinder;
import eg.edu.alexu.csd.oop.draw.DrawingBoard;
import eg.edu.alexu.csd.oop.draw.Factory;
import eg.edu.alexu.csd.oop.draw.MyEngine;
import eg.edu.alexu.csd.oop.draw.Shape;
import eg.edu.alexu.csd.oop.draw.gui.Gui;
import eg.edu.alexu.csd.oop.draw.gui.listners.*;

/**
 * The Class Controller.
 */
public class Controller {

  /**
   * Instantiates a new controller.
   */
  public Controller() {

  }

  /**
   * Start application.
   */
  public void startApplication() {
    MyEngine engine = new MyEngine();
    Factory factory = Factory.getInstance();
    factory.addShapes(engine.getSupportedShapes());
    Gui guiWindow = new Gui(new DrawingBoard(engine));
    guiWindow.getFrame().setVisible(true);
    guiWindow.makeShapeButtons(engine.getSupportedShapes());
    ClassFinder<Shape> myClassFinder = new ClassFinder<Shape>(Shape.class);
    SaveActionListner saveActionListner = new SaveActionListner(engine);
    LoadActionListener loadActionListener = new LoadActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas());
    UndoActionListener undoActionListener = new UndoActionListener(engine, guiWindow.getPanel(),
        guiWindow, guiWindow.getCanvas());
    RedoActionListener redoActionListener = new RedoActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu());

    DeleteActionListener deleteActionListener = new DeleteActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu());
    MovingActionListener movingActionListener = new MovingActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu());
    ResizeAction resizeActionListener = new ResizeAction(guiWindow.getMenu(), guiWindow.getPanel(),
        engine, guiWindow.getCanvas(), guiWindow);
    ShapeButtonListener[] shapeButtonListener = new ShapeButtonListener[guiWindow
        .getShapesButtons().length];
    for (int counter = 0; counter < shapeButtonListener.length; counter++) {
      shapeButtonListener[counter] = new ShapeButtonListener(engine, guiWindow,
          guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu(),
          guiWindow.getShapesButtons()[counter], factory);
    }
    AboutActionListener aboutActionListener = new AboutActionListener();
    ColorActionListener colorActionListener = new ColorActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu());
    CopyActionListener copyActionListener = new CopyActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu());
    InstallActionListener installActionListener = new InstallActionListener(engine, guiWindow,
        guiWindow.getPanel(), guiWindow.getCanvas(), guiWindow.getMenu(), factory, myClassFinder);
    guiWindow.addButtonsActionListeners(loadActionListener, undoActionListener, saveActionListner,
        resizeActionListener, deleteActionListener, movingActionListener, redoActionListener,
        installActionListener, aboutActionListener, colorActionListener, copyActionListener);
    guiWindow.createShapeButtons(engine.getSupportedShapes(), shapeButtonListener);
  }

}
