package eg.edu.alexu.csd.oop.draw.gui;

import eg.edu.alexu.csd.oop.draw.DrawingBoard;
import eg.edu.alexu.csd.oop.draw.Shape;
import eg.edu.alexu.csd.oop.draw.gui.listners.AboutActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.ColorActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.CopyActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.DeleteActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.InstallActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.LoadActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.MenuActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.MovingActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.RedoActionListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.ResizeAction;
import eg.edu.alexu.csd.oop.draw.gui.listners.SaveActionListner;
import eg.edu.alexu.csd.oop.draw.gui.listners.ShapeButtonListener;
import eg.edu.alexu.csd.oop.draw.gui.listners.UndoActionListener;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class MainWindow.
 */
public class Gui {

  /** The frame. */
  private Box shapesBox;

  /** The frame. */
  private JFrame frame;

  /** The edit buttons. */
  private JButton delete, resize, color, moveTo, copy, undo, redo;

  /** The saving and plug-in buttons. */
  private JButton save, load, install, about;

  /** The panel. */
  private JPanel panel;

  /** The menu. */
  private JComboBox<Object> menu;

  /** The canvas. */
  private DrawingBoard canvas;

  /** The buttons panel. */
  private JPanel buttonsPanel;

  /** The shapes buttons. */
  private JButton[] shapesButtons;

  /**
   * Create the application.
   *
   * @param canvas
   *          the canvas
   */
  public Gui(DrawingBoard canvas) {
    this.canvas = canvas;
    initialize();
  }

  /**
   * Gets the frame.
   *
   * @return the frame
   */

  public JFrame getFrame() {
    return frame;
  }

  /**
   * Gets the panel.
   *
   * @return the panel
   */
  public JPanel getPanel() {
    return panel;
  }

  /**
   * Gets the canvas.
   *
   * @return the canvas
   */
  public DrawingBoard getCanvas() {
    return canvas;
  }

  /**
   * Gets the menu.
   *
   * @return the menu
   */
  public JComboBox<Object> getMenu() {
    return menu;
  }

  /**
   * Gets the install button.
   *
   * @return the install button
   */
  public JButton getInstallButton() {
    return install;
  }

  /**
   * Gets the shapes buttons.
   *
   * @return the shapes buttons
   */
  public JButton[] getShapesButtons() {
    return shapesButtons;
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    initializeFrame();

  }
  /*
   * END of initialize
   * 
   */

  /**
   * Refresh menu.
   *
   * @param existingShapes
   *          the existing shapes
   */
  public void refreshMenu(Shape[] existingShapes) {
    menu.removeAllItems();
    String choose = "Choose Shape";
    menu.addItem(choose);
    for (Shape dummyShape : existingShapes) {
      menu.addItem(dummyShape);
    }
  }

  /**
   * Make shape buttons.
   *
   * @param shapeList
   *          the shape list
   */
  public void makeShapeButtons(List<Class<? extends Shape>> shapeList) {
    if (shapesBox != null) {
      frame.remove(shapesBox);
    }
    shapesBox = Box.createHorizontalBox();
    shapesBox.setBounds(222, 11, 962, 27);

    shapesButtons = new JButton[shapeList.size()];
    int counter = 0;
    for (Class<? extends Shape> shape : shapeList) {
      shapesButtons[counter] = new JButton(shape.getSimpleName());
      shapesButtons[counter].setToolTipText("Draw a " + shape.getSimpleName());
      shapesBox.add(shapesButtons[counter]);
      counter++;
    }
    frame.getContentPane().add(shapesBox);
    shapesBox.setVisible(true);
  }

  /**
   * Creates the shape buttons.
   *
   * @param shapeList
   *          the shape list
   * @param buttonActionListeners
   *          the button action listeners
   */
  public void createShapeButtons(List<Class<? extends Shape>> shapeList,
      ShapeButtonListener[] buttonActionListeners) {
    if (shapesBox != null) {
      frame.remove(shapesBox);
    }
    shapesBox = Box.createHorizontalBox();
    shapesBox.setBounds(222, 11, 962, 27);

    JButton[] shapesButtons = new JButton[shapeList.size()];
    int counter = 0;
    for (Class<? extends Shape> shape : shapeList) {
      shapesButtons[counter] = new JButton(shape.getSimpleName());
      shapesButtons[counter].addActionListener(buttonActionListeners[counter]);
      shapesButtons[counter].setToolTipText("Draw a " + shape.getSimpleName());
      shapesBox.add(shapesButtons[counter]);
      counter++;
    }
    frame.getContentPane().add(shapesBox);
    shapesBox.setVisible(true);
    // frame.repaint();
  }

  /**
   * Removes the shapes box.
   */
  /*
   * END update the menu
   */
  public void removeShapesBox() {
    shapesBox.setVisible(false);
  }

  /**
   * Adds the buttons.
   *
   * @param buttonsPanel
   *          the buttons panel
   */
  public void addButtons(JPanel buttonsPanel) {
    resize = new JButton("Resize");
    resize.setBounds(105, 235, 91, 23);
    resize.setEnabled(false);
    buttonsPanel.add(resize);
    save = new JButton("Save");
    save.setBounds(105, 45, 91, 23);
    buttonsPanel.add(save);
    undo = new JButton("Undo");
    undo.setBounds(10, 11, 85, 23);
    buttonsPanel.add(undo);
    load = new JButton("Load");
    load.setBounds(10, 45, 85, 23);
    buttonsPanel.add(load);
    delete = new JButton("delete");
    delete.setBounds(10, 303, 186, 23);
    delete.setEnabled(false);
    buttonsPanel.add(delete);
    color = new JButton("Color");
    color.setBounds(10, 235, 85, 23);
    color.setEnabled(false);
    buttonsPanel.add(color);
    moveTo = new JButton("Cut");
    moveTo.setBounds(10, 269, 85, 23);
    moveTo.setEnabled(false);
    buttonsPanel.add(moveTo);
    copy = new JButton("Copy");
    copy.setBounds(105, 269, 91, 23);
    copy.setEnabled(false);
    buttonsPanel.add(copy);
    redo = new JButton("Redo");
    redo.setBounds(105, 11, 91, 23);
    buttonsPanel.add(redo);
    install = new JButton("Install Shape");
    install.setBounds(10, 460, 186, 23);
    buttonsPanel.add(install);
    about = new JButton("About");
    about.setBounds(10, 494, 186, 23);
    buttonsPanel.add(about);
  }

  /**
   * Adds the buttons action listeners.
   *
   * @param loadActionListener
   *          the load action listener
   * @param undoActionListener
   *          the undo action listener
   * @param saveActionListener
   *          the save action listener
   * @param resizeActionListener
   *          the resize action listener
   * @param deleteActionListener
   *          the delete action listener
   * @param movingActionListener
   *          the moving action listener
   * @param redoActionListener
   *          the redo action listener
   * @param installActionListener
   *          the install action listener
   * @param aboutActionListener
   *          the about action listener
   * @param colorActionListener
   *          the color action listener
   * @param copyActionListener
   *          the copy action listener
   */
  public void addButtonsActionListeners(LoadActionListener loadActionListener,
      UndoActionListener undoActionListener, SaveActionListner saveActionListener,
      ResizeAction resizeActionListener, DeleteActionListener deleteActionListener,
      MovingActionListener movingActionListener, RedoActionListener redoActionListener,
      InstallActionListener installActionListener, AboutActionListener aboutActionListener,
      ColorActionListener colorActionListener, CopyActionListener copyActionListener) {
    load.addActionListener(loadActionListener);
    load.setToolTipText("Load a saved file");
    undo.addActionListener(undoActionListener);
    undo.setToolTipText("return to a old move");
    save.addActionListener(saveActionListener);
    save.setToolTipText("Save the canvas in file");
    resize.addActionListener(resizeActionListener);
    resize.setToolTipText("Change the size of selected shape");
    delete.addActionListener(deleteActionListener);
    delete.setToolTipText("Delete the shape");
    color.addActionListener(colorActionListener);
    color.setToolTipText("Change the stroke and fill color of the shape");
    moveTo.addActionListener(movingActionListener);
    moveTo.setToolTipText("Move the shape to another place");
    copy.addActionListener(copyActionListener);
    copy.setToolTipText("Make a copy of the shape");
    redo.addActionListener(redoActionListener);
    redo.setToolTipText("Return The undo moves");
    install.addActionListener(installActionListener);
    install.setToolTipText("Add a new shapes");
    about.addActionListener(aboutActionListener);
    about.setToolTipText("Know who we are");
  }

  /**
   * Initialize frame.
   */
  private void initializeFrame() {
    frame = new JFrame();
    frame.setSize(1200, 730);
    frame.setLocationRelativeTo(null);
    frame.setTitle("Vector Drawing Application");
    frame.setResizable(false);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    buttonsPanel = new JPanel();
    buttonsPanel.setBounds(10, 48, 206, 643);
    frame.getContentPane().setLayout(null);
    panel = new JPanel();
    panel.setSize(962, 643);
    panel.setLocation(222, 48);
    panel.setBackground(Color.white);
    frame.getContentPane().add(panel);
    frame.getContentPane().add(buttonsPanel);
    buttonsPanel.setLayout(null);
    menu = new JComboBox<Object>(new Object[] { "Choose Shape" });
    menu.setBounds(10, 146, 186, 23);
    buttonsPanel.add(menu);
    addButtons(buttonsPanel);
    menu.addItemListener(new MenuActionListener(copy, moveTo, color, delete, resize, menu));
    JLabel selectShape = new JLabel("Select Shape");
    selectShape.setBounds(10, 112, 186, 23);
    buttonsPanel.add(selectShape);
    JLabel lblShapes = new JLabel("Shapes");
    lblShapes.setFont(new Font("Arial Black", Font.BOLD, 14));
    lblShapes.setBounds(10, 11, 206, 27);
    frame.getContentPane().add(lblShapes);
  }

}
